package database;

import java.io.Serializable;

public class Estados implements Serializable {
    private int Id;
    private String Nombre;
    private String idmovil;

    public Estados() {
        this.Id = 0;
        this.Nombre = "";
        this.idmovil = "";
    }

    public Estados(int id, String nombre) {
        Id = id;
        Nombre = nombre;
        idmovil = idmovil;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getIdmovil(){return idmovil;}

    public void setIdmovil(String idmovil) {this.idmovil = idmovil;}

}
