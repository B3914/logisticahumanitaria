package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LogisticaDbHelper extends SQLiteOpenHelper {
    private SQLiteDatabase db;
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ESTADO = "CREATE TABLE " +
            DefinirTabla.Estado.TABLE_NAME + " (" +
            DefinirTabla.Estado._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Estado.COLUMN_NAME_NOMBRE + TEXT_TYPE +
            DefinirTabla.Estado.COLUMN_NAME_IDMOVIL + TEXT_TYPE + ")";

    private static final String SQL_DELETE_ESTADO = "DROP TABLE IF EXISTS " + DefinirTabla.Estado.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "logistica.db";

    public LogisticaDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion) {
        db.execSQL(SQL_DELETE_ESTADO);
        onCreate(db);
        insertarEstado();
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int
            newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ESTADO);
    }

    public void insertarEstado(){
        String Estados[] = {"Sinaloa", "Nuevo León", "Jalisco", "Zacatecas", "Tlaxcala", "Sonora", "Chihuahua", "Coahuila", "Chiapas", "Veracruz"};
        ContentValues values =new ContentValues();
        for(int x=0; x<10; x++){
            values.clear();
            values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE,Estados[x]);
            db.insert(DefinirTabla.Estado.TABLE_NAME,null,values);
        }

    }

}

