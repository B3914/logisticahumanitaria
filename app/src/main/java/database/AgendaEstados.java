package database;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
public class AgendaEstados {
    Context context;
    LogisticaDbHelper myDbHelper;
    SQLiteDatabase db;
    String[] columnas = new String[]{
            DefinirTabla.Estado._ID,
            DefinirTabla.Estado.COLUMN_NAME_NOMBRE,
            DefinirTabla.Estado.COLUMN_NAME_IDMOVIL
    };

    public AgendaEstados(Context context){
        this.context = context;
        this.myDbHelper = new LogisticaDbHelper(this.context);
    }

    public void openDatabase(){
        db = myDbHelper.getWritableDatabase();
    }

    public long insertContacto(Estados c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_IDMOVIL, c.getIdmovil());
        return db.insert(DefinirTabla.Estado.TABLE_NAME, null, values);
        //regresa el id insertado
    }

    public long updateContacto(Estados c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_IDMOVIL, c.getIdmovil());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estado.TABLE_NAME , values,
                DefinirTabla.Estado._ID + " = " + id,null);
    }

    public int deleteEstado(long id){
        return db.delete(DefinirTabla.Estado.TABLE_NAME,DefinirTabla.Estado._ID + "=?",
                new String[]{ String.valueOf(id) });
    }

    private Estados readContacto(Cursor cursor){
        Estados c = new Estados();
        c.setId(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setIdmovil(cursor.getString(2));
        return c;
    }

    public void close(){
        myDbHelper.close();
    }

    public Estados getContacto(long id){
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnas,
                DefinirTabla.Estado._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estados contacto = readContacto(c);
        c.close();
        return contacto;
    }

    public ArrayList<Estados> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnas, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
}