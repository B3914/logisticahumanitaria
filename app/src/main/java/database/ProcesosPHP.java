package database;
import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>,Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> contactos = new ArrayList<Estados>();
    private String serverip = "https://logisticahumanitariaeq6.000webhostapp.com/WebService/";
    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }
    public void insertarContactoWebService(Estados e){
        String url = serverip + "wsRegistro.php?estado="+e.getNombre()+"&idMovil="+e.getIdmovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void actualizarContactoWebService(Estados e,String nombre){
        String url = serverip + "wsActualizar.php?antes="+nombre+"&despues="+e.getNombre()+"&idMovil="+e.getIdmovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void borrarContactoWebService(String nombre){
        String url = serverip + "wsEliminar.php?nombre="+nombre;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public ArrayList<Estados> mostrarTodosWebService(Context context)
    {
        String url = serverip + "wsConsultarTodos.php?idMovil="+Device.getSecureId(context);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }

    public ArrayList<Estados> mostrarContactoWebService(Context context)
    {
        String url = serverip + "wsConsultarContacto.php?idMovil="+Device.getSecureId(context);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }
    @Override
    public void onResponse(JSONObject response) {
        this.contactos.removeAll(this.contactos);
        Estados contacto = null;
        JSONArray json = response.optJSONArray("contactos");
        try {
            for (int i = 0; i < json.length(); i++) {
                contacto = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                contacto.setId(jsonObject.optInt("id"));
                contacto.setNombre(jsonObject.optString("nombre"));
                contacto.setIdmovil(jsonObject.optString("idMovil"));
                contactos.add(contacto);
            }
            //MyArrayAdapter adapter = new MyArrayAdapter(context, R.layout.layout_contacto, listaContactos);
            //setListAdapter(adapter);
            //lst.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
