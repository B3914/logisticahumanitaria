package com.example.logisticahumanitaria;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import database.AgendaEstados;
import database.Estados;
import database.ProcesosPHP;


public class activity_lista extends ListActivity {
    private AgendaEstados logisticaEstado;
    ProcesosPHP php;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        this.php=new ProcesosPHP();
        php.setContext(this);
        Button btnNuevo =(Button) findViewById(R.id.btnNuevo);
        logisticaEstado = new AgendaEstados(this);
        logisticaEstado.openDatabase();
        ArrayList<Estados> estados = logisticaEstado.allContactos();
        MyArrayAdapter adapter = new MyArrayAdapter (this, R.layout.activity_estado, estados);
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    class MyArrayAdapter extends ArrayAdapter<Estados> {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreEstado);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getNombre());
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(isNetworkAvailable()) {
                        php.borrarContactoWebService(objects.get(position).getNombre());
                        logisticaEstado.openDatabase();
                        logisticaEstado.deleteEstado(objects.get(position).getId());
                        logisticaEstado.close();
                        objects.remove(position);
                        notifyDataSetChanged();
                    }
                }
            });

            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("estado", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
}